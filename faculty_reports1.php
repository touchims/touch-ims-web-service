<?php
require_once('dbconnect.php');
require_once('sync.php');
require_once('staticvalues.php');
require_once('timebetween.php');
$offer_code = $_POST['offercode'];






$db = new DBconnect();
$connection=$db->connect();
$staticvalues = new StaticValues();
$fr = new FacultyReports($connection,$staticvalues);
//$fr->button_labels(3002);

$timebetween = new TimeBetween($connection,$staticvalues);


if(isset($_POST['username'])){
	$username = $_POST['username'];
}

//isset($_POST['action']) && $_POST['action']=="enable"
if(isset($_POST['action']) && $_POST['action']=="enable"){
	//echo $offer_code;
	if(!$fr->checkDate_query($offer_code)){
		$current = $timebetween->timebetween2();
		$subject_time = $fr->getgetTime($offer_code);


		//echo "enabled";
		if($current!=$subject_time)
			echo "Disabled";
		else
			echo "enabled";
	}
	else
		echo "disabled";

}

if(isset($_POST['button_labels'])){
	$fr->button_labels($offer_code);
}

//$fr->report('1001',"CHECKMARK",'glendon');


if(isset($_POST['action']) && ($_POST['action']=="CHECKMARK" || $_POST['action']=="XMARK")){
	$fr->report($offer_code,$_POST['action'],$username);
}

if(isset($_POST['action']) && $_POST['action']=="SAVE"){
	$fr->hitSave($offer_code,$username);


}




class FacultyReports{
	private $connection;
	private $time;
	private $date;
	function __construct($connection){
		$this->connection = $connection;
		$staticvalues = new StaticValues();
		$this->time = $staticvalues->getTime();
		$this->date = $staticvalues->getDate();
	}
	function getgetTime($offer_code){
		$query = "select substr(time,-13,5) as time_start, substr(time,-5) as time_end 
		from subj_schedule where offer_code=$offer_code";
		$result = mysqli_query($this->connection,$query);
		$results = mysqli_fetch_assoc($result);

		return $results['time_start']." - ".$results['time_end'];
	}
	function generateReportID(){

		$report_id = rand( 1000000 ,99999999);

		$query="select report_id from touch_faculty_report where report_id='$report_id'";
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows>0)
			$this->generateReportID();

		else
			return $report_id;
	}

	function setPending($offer_code){

		$date = $date =date('Y-m-d');
	}
	function hitSave($offer_code,$username){
		$date = $this->date;
		$time = $this->time;
		$message = false;
		$status = $this->getStatus($offer_code);
		if($status=='0/2'){
			$query = "update touch_faculty_report set checking_status='1/2', checker1_time='$date $time',
			checker1='$username'
			where offer_code='$offer_code' and date='$date' and (first_checking='PRESENT' or first_checking='ABSENT' or first_checking='LATE')";
			$result = mysqli_query($this->connection,$query);
			$sync = new Sync($this->connection);
			$sync->insertSync('Commited Transaction1:$offer_code',$username);
			//echo $query;
			echo "Transaction 1 DONE";

			$query = "select report_id, first_checking from touch_faculty_report where date='$date' and offer_code=$offer_code";
			$result = mysqli_query($this->connection,$query);
			$results = mysqli_fetch_assoc($result);
			$report_id = $results['report_id'];
			$first_checking = $results['first_checking'];

			if($first_checking=='ABSENT' || $first_checking=='LATE'){
				// $query = "insert into notifications
				// (report_id,transaction)
				// values('$report_id',1)";
				// $result = mysqli_query($this->connection,$query);
				// $notify= true;

				$mobile= $this->getMobileNo($offer_code);
				$textmessage = "you were marked as $first_checking by $username ";
				if(!empty($mobile))
					$this->sendSMS($mobile,$textmessage,$report_id);

			}
		}
		else{
			$query = "update touch_faculty_report set checking_status='2/2', checker2_time=now(),
			checker2='$username'
			where offer_code='$offer_code' and date='$date' ";
			$result = mysqli_query($this->connection,$query);
			$sync = new Sync($this->connection);
			$sync->insertSync('Commited Transaction2:$offer_code',$username);
			echo "Transaction 2 DONE";


			$query = "select report_id, last_checking from touch_faculty_report where date='$date' and offer_code=$offer_code";
			$result = mysqli_query($this->connection,$query);
			$results = mysqli_fetch_assoc($result);
			$report_id = $results['report_id'];
			$last_checking = $results['last_checking'];

			if($last_checking=='EARLY DISMISSAL'){
				// $query = "insert into notifications
				// (report_id,transaction)
				// values('$report_id',2)";
				// $result = mysqli_query($this->connection,$query);
				// $notify = true;

				$mobile= $this->getMobileNo($offer_code);
				$textmessage = "you were marked as $last_checking by $username";
				if(!empty($mobile))
					$this->sendSMS($mobile,$textmessage,$report_id);

			}
		}
	}

	function getMobileNo($offer_code){
				$query = "select a.mobile from touch_ims_accounts a , person p, teacher t, subj_schedule s 
				where p.person_id = t.person_id and t.teacher_id = s.teacher_id and 
				a.person_id = p.person_id and s.offer_code=$offer_code";
				$result = mysqli_query($this->connection,$query);
				$results = mysqli_fetch_assoc($result);
				return $results['mobile'];

	}


	function sendSMS($mobile_number,$message,$message_id){

		require_once('ChikkaSMS.php');
		$clientId = 'f642cce7d842cb3461f5d671a701a9602d35d1e0bd49b61d33a561b0969579c0';
		$secretKey = 'a38b3a8faa47248bb4b98620432f84441e560c39e1b29c25babf8fb8f27fc91e';
		$shortCode = '2929086127';
		$chikkaAPI = new ChikkaSMS($clientId,$secretKey,$shortCode);
		$response = $chikkaAPI->sendText($message_id, $mobile_number, $message);
	}
	function minutesElapsed($offer_code,$mark){
		$query="select substr(time,-13,5) as time_start, substr(time,-5) as time_end from subj_schedule 
		where offer_code='$offer_code'";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}

	   	//$start_time = strtotime("14:30"); //static
		//$current_time = strtotime("14:33"); //static
		$start_time = strtotime($results[0]['time_start']); //from db
		$current_time = strtotime($this->time);


		$minutes =  round(($current_time - $start_time) / 60,2);
		$message = null;

		//echo $minutes;

		if($mark == 'CHECKMARK'){
			if($minutes<5 && $minutes >=0)
				$message= "PRESENT";
			elseif($minutes>=5 && $minutes<15)
				$message = "LATE";
			else
				$message = "LOCK";


				
		}
		if($mark == 'XMARK')
			$message = "ABSENT";
		
		return $message;

	}

	function minutesBeforeOut($offer_code,$mark){

		$firstchecking = $this->firstchecking($offer_code);

		$query="select substr(time,-13,5) as time_start, substr(time,-5) as time_end from subj_schedule 
		where offer_code='$offer_code'";
		$result = mysqli_query($this->connection,$query);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
	
	    $end_time = strtotime($results[0]['time_end']); //from db
		$current_time = strtotime($this->time);
		$minutes =  round(($end_time - $current_time) / 60,2);
		$message = null;

		if($minutes<=5 && $minutes>0){
			if($firstchecking=="PRESENT" || $firstchecking=="LATE"){
				if($mark == 'CHECKMARK'){
					$message= "OK";
				}
				if($mark == 'XMARK'){
					$message= "EARLY DISMISSAL";
				}
			}
			else{
				if($mark == 'CHECKMARK'){
					$message= "HELD CLASSES";
				}
				if($mark == 'XMARK'){
					$message= "ABSENT";
				}
			}
		}
		else
			$message='LOCK';

		return $message;
	}

	function button_labels($offer_code){
		$status = $this->getStatus($offer_code);
		$firstchecking = $this->firstChecking($offer_code);


		$label['checkmark']=null;
		$label['xmark']=null;
//strlen($firstchecking)==0 
		if($status=="0/2"){
			//echo $firstchecking;
			$label['checkmark']=$this->minutesElapsed($offer_code,'CHECKMARK');
			$label['xmark']=$this->minutesElapsed($offer_code,'XMARK');

		}
		if($status=="1/2"){

			$label['checkmark']=$this->minutesBeforeOut($offer_code,'CHECKMARK');
			$label['xmark']=$this->minutesBeforeOut($offer_code,'XMARK');

		}

		//if($firstchecking=="PRESENT" && $status=="1/2")

		
		// if($status=='1/2'){

		// $label['checkmark']=$this->minutesBeforeOut($offer_code,'CHECKMARK');
		// 	$label['xmark']=$this->minutesBeforeOut($offer_code,'XMARK');
		// }



		
		echo json_encode($label);

	}

	function checkDate_query($offer_code){

		$date = $this->date;
		$query = "select * from touch_faculty_report 
		where date='$date' and offer_code='$offer_code' and checking_status='2/2'";
		$result = mysqli_query($this->connection,$query);
		$rows = mysqli_num_rows($result);
		if($rows>0)
			return true;
		else
			false;

	}

	function countDate($offer_code){
		$date = $this->date;
		$query = "select count(date)as count from touch_faculty_report where date='$date' and offer_code='$offer_code'";
		$result = mysqli_query($this->connection,$query);
		$output = mysqli_fetch_assoc($result);
		return $output['count'];
	}

	function firstChecking($offer_code){
		$date = $this->date;
		$query = "
		select first_checking from touch_faculty_report where offer_code='$offer_code' and date='$date'
		";
		$result = mysqli_query($this->connection,$query);
		$output = mysqli_fetch_assoc($result);
		
		return $output['first_checking'];
	}



	function updateReport($offer_code,$action,$username){
		

		$message = $this->minutesElapsed($offer_code,$action);
		$message2 = $this->minutesBeforeOut($offer_code,$action);
		$query = null;
		$report_id = $this->generateReportID();
		$status = $this->getStatus($offer_code);
		
		$checkDate = $this->countDate($offer_code);
		$date = $this->date;

		//echo $message;

		$sync = new Sync($this->connection);

		if($status=="0/2"){

			if($checkDate!=1)
				$query = "insert into touch_faculty_report (report_id,first_checking,date,offer_code) 
			values ('$report_id', '$message' , '$date' , '$offer_code' )";		
			else
				$query = "update touch_faculty_report set first_checking='$message' 
			where date='$date' and offer_code='$offer_code'";

			
			$sync->insertSync('Uncommited Transaction1',$username);
			

		}
		else{
			$query = "update touch_faculty_report set last_checking='$message2' 
			where date='$date' and offer_code='$offer_code'";

			
			$sync->insertSync('Uncommited Transaction2',$username);

		}


		
		$result = mysqli_query($this->connection,$query);

		return $result;

	}

	function report($offer_code,$action,$username){

		

		$this->updateReport($offer_code,$action,$username);

	}

	function getStatus($offer_code){
		$date = $this->date;
		$status=null;
		$query = "select checking_status from touch_faculty_report 
		where offer_code='$offer_code' and date='$date'";
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		if($rows){
			
			$results = array();
			while($line = mysqli_fetch_assoc($result)){
				$results[] = $line;
			}
			$status = $results[0]['checking_status'];
		}
		else{
			$status = "0/2";
		}
		return $status;


	}

}
?>


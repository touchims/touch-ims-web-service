<?php


require_once('staticvalues.php');

class TodayIs {
    
    function printDay(){

        
        $day = $this->dayoftheweek();

        if($day==0){
            echo "Sunday";
        }
        elseif($day==1){
            echo "Monday";
        }
        elseif($day==2){
            echo "Tuesday";
        }
        elseif($day==3){
            echo "Wednesday";
        }
        elseif($day==4){
            echo "Thursday";
        }
        elseif($day==5){
            echo "Friday";
        }
        elseif($day==6){
            echo "Saturday";
        }
    }


    function dayoftheweek(){
        $staticvalues = new StaticValues();
        $dayoftheweek = $staticvalues->dayoftheweek();
       //$dw = date( "w"); //for static purposes
        $dw = $dayoftheweek;
       return $dw;
       
       
    }
    
    function getInit(){
        $dw = $this->dayoftheweek();
        
        if($dw==0){
            return "Sun";
        }
        elseif($dw==1){
            return "%M%";
        }
        elseif($dw==2){
            return "%T%' and day not like 'Th%' and day not like 'MTh%' and day not like 'MWTh%' and day not like 'WTh%";
        }
        elseif($dw==3){
            return "%W%";
        }
        elseif($dw==4){
            return "%Th%";
        }
        elseif($dw==5){
            return "%F%";
        }
        elseif($dw==6){
            return "%Sat%";
        }
        
        
        
    }
    

}


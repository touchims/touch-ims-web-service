<?php
/* Execute a prepared statement by binding PHP variables */


$colour = "red";
$dbh = new PDO('mysql:host=localhost;dbname=touchims','root','' );

$sth = $dbh->prepare('SELECT * from person');

$sth->bindParam(':colour', $colour, PDO::PARAM_STR, 12);
$sth->execute();
$result = $sth->fetch(PDO::FETCH_ASSOC);

var_dump($result);
<?php
require_once("staticvalues.php");
require_once("dbconnect.php");


$db = new dbconnect();
$connection=$db->connect();
$staticvalues = new StaticValues();

//offer_code 1001

// $timebetween = new TimeBetween($connection,$staticvalues);
// $timebetween->timebetween();


class TimeBetween{
	private $connection;
	private $staticvalues;

	function __construct($connection,$staticvalues){
		$this->connection = $connection;
		$this->staticvalues = $staticvalues;

	}

	function query($query){
		$result = mysqli_query($this->connection,$query);
		$rows=mysqli_num_rows($result);
		$results = array();
		while($line = mysqli_fetch_assoc($result)){
			$results[] = $line;
		}
		return $results;

	}

	function timeBetween(){
		$time_query=$this->query("SELECT substr(time,1,5)as start_time, 
			substr(time,8,11) as end_time FROM `subj_schedule` order by time asc");

		$current_time = $this->staticvalues->getTime();
		$target_time = null;
		for($i=0; $i<sizeof($time_query); $i++){
			$start = $time_query[$i]['start_time'];
			$end = $time_query[$i]['end_time'];

			if(strtotime($current_time)>=strtotime($start) && strtotime($current_time)<=strtotime($end)){
				$target_time = "$start -$end"; //take care
			}
		}
		if($target_time==null)
			echo "no classes";
		else
			echo $target_time;
	}
	function timeBetween2(){
		$time_query=$this->query("SELECT substr(time,1,5)as start_time, 
			substr(time,8,11) as end_time FROM `subj_schedule` order by time asc");

		$current_time = $this->staticvalues->getTime();
		$target_time = null;
		for($i=0; $i<sizeof($time_query); $i++){
			$start = $time_query[$i]['start_time'];
			$end = $time_query[$i]['end_time'];

			if(strtotime($current_time)>=strtotime($start) && strtotime($current_time)<=strtotime($end)){
				$target_time = "$start -$end"; //take care
			}
		}
		if($target_time==null)
			return "no classes";
		else
			return $target_time;
	}

}






?>